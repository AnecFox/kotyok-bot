import random

from aiogram import Bot, Dispatcher, executor, types

import config

bot = Bot(token=config.tg_bot_token)
dp = Dispatcher(bot)

meows = ["Мяу", "Мяф", "Мяфк", "Мя", "Meow"]


@dp.message_handler()
async def meow(message: types.Message):
    random_meow_index = random.randint(0, len(meows) - 1)
    meow_message = meows[random_meow_index]

    await message.answer(meow_message)


if __name__ == "__main__":
    executor.start_polling(dp)
